<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresasTable extends Migration
{
    public function up()
    {
        Schema::create('empresas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('cnpj',14)->unique();
            $table->string('endereco');
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('empresas');
    }
}
