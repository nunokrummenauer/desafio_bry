@extends('layouts.app', ["current" => "users"])
@section('body')
<div class="card border">
  <div class="card-body">
    <h5 class="card-title">Usuários Cadastrados</h5>
    <div class="table-responsive">
      <table class="table table-ordered table-hover">
        <tr>
          <th>Nome</th>
          <th>CPF</th>
          <th></th>
          <th></th>
          <th></th>
        </tr>
        @foreach ($User as $User)
          <tr>
            <td> {{$User->name}}</td>
            <td> {{$User->cpf}}</td>
            <td class="text-center">
              <a href= "{{ route('user.edit',$User->id) }}" class="btn btn-sm">
                Editar</i></a>
            </td>
            <td class="text-center">
              <a href= "{{ route('user.show',$User->id) }}" class="btn btn-sm">
                Informações</i></a>
            </td>
            <td class="text-center">
              {{method_field('DELETE')}}
              <a href= "{{ route('user.destroy',$User->id) }}" value="Delete" class="btn btn-sm">
                Excluir</i></a>
            </td>
          </tr>
        @endforeach
      </table>
      <a class="btn btn-danger" href="{{ route('user.create') }}">Cadastrar Novo Usuário</a>
    </div>
  </div>
</div>

@endsection
