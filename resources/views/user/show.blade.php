@extends('layouts.app', ["current" => "users"])

@section('body')
<div class="card border">
  <div class="card-body">
    <h5 class="card-title">Dados do Usuário</h5>
    <form class="form-horizontal" role="form" method="POST" action="#">
      <div class="form-group">
        <label for="user" class="col-md-4 control-label">Nome do Usuário</label>
        <label for="user" class="col-md-6 control-label text-left">{{ $User->name }}</label>
      </div>
      <div class="form-group">
        <label for="nome" class="col-md-4 control-label">CPF</label>
        <label for="nome" class="col-md-6 control-label text-left">{{ $User->cpf }}</label>
      </div>
      <div class="form-group">
        <label for="nome" class="col-md-4 control-label">login</label>
        <label for="nome" class="col-md-6 control-label text-left">{{ $User->login }}</label>
      </div>
      <div class="form-group">
        <label for="nome" class="col-md-4 control-label">email</label>
        <label for="nome" class="col-md-6 control-label text-left">{{ $User->email }}</label>
      </div>
      <div class="form-group">
        <label for="nome" class="col-md-4 control-label">Endereço</label>
        <label for="nome" class="col-md-6 control-label text-left">{{ $User->endereco }}</label>
      </div>
    </form>
  </div>
</div>
@endsection
