@extends('layouts.app', ["current" => "users"])
@section('body')
<div class="card border">
  <div class="card-body">
    <form class="form-horizontal" role="form" method="POST" action="{{ route('user.update',$User->id) }}">
      @csrf
      @method('PUT')
      <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        <label for="name" class="col-md-4 control-label">Nome </label>
        <div class="col-md-6">
          <input id="name" type="text" class="form-control" name="name" value="{{$User->name}}" required>
          @if ($errors->has('name'))
            <span class="help-block">
              <strong>{{ $errors->first('name') }}</strong>
            </span>
          @endif
        </div>
      </div>
      <div class="form-group{{ $errors->has('cnpj') ? ' has-error' : '' }}">
        <label for="cpf" class="col-md-4 control-label">CPF </label>
        <div class="col-md-6">
          <input id="cpf" type="text" class="form-control" name="cpf" value="{{$User->cpf}}" required>
          @if ($errors->has('cpf'))
            <span class="help-block">
              <strong>{{ $errors->first('cpf') }}</strong>
            </span>
          @endif
        </div>
      </div>
      <div class="form-group{{ $errors->has('login') ? ' has-error' : '' }}">
        <label for="login" class="col-md-4 control-label">Login </label>
        <div class="col-md-6">
          <input id="login" type="text" class="form-control" name="login" value="{{$User->login}}" required>
          @if ($errors->has('login'))
            <span class="help-block">
              <strong>{{ $errors->first('login') }}</strong>
            </span>
          @endif
        </div>
      </div>
      <div class="form-group{{ $errors->has('login') ? ' has-error' : '' }}">
        <label for="email" class="col-md-4 control-label">Email </label>
        <div class="col-md-6">
          <input id="email" type="text" class="form-control" name="email" value="{{$User->email}}" required>
          @if ($errors->has('email'))
            <span class="help-block">
              <strong>{{ $errors->first('login') }}</strong>
            </span>
          @endif
        </div>
      </div>
      <div class="form-group{{ $errors->has('endereco') ? ' has-error' : '' }}">
        <label for="endereco" class="col-md-4 control-label">Endereco </label>
        <div class="col-md-6">
          <input id="endereco" type="text" class="form-control" name="endereco" value="{{$User->endereco}}">
          @if ($errors->has('endereco'))
            <span class="help-block">
              <strong>{{ $errors->first('endereco') }}</strong>
            </span>
          @endif
        </div>
      </div>
      <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
          <button type="submit" class="btn btn-primary">
            <i class="fa fa-save"></i> Salvar
          </button>
        </div>
      </div>
    </form>
  </div>
</div>
@endsection
