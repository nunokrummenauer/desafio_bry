@extends('layouts.app', ["current" => "users"])
@section('body')
<div class="card border">
  <div class="card-body">
    <form class="form-horizontal" role="form" method="POST" action="{{ route('user.store') }}">
      @csrf
      <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        <label for="name" class="col-md-4 control-label">Nome </label>
        <div class="col-md-6">
          <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required>
          @if ($errors->has('name'))
            <span class="help-block">
              <strong>{{ $errors->first('name') }}</strong>
            </span>
          @endif
        </div>
      </div>
      <div class="form-group{{ $errors->has('cnpj') ? ' has-error' : '' }}">
        <label for="cpf" class="col-md-4 control-label">CPF </label>
        <div class="col-md-6">
          <input id="cpf" type="text" class="form-control" name="cpf" value="{{ old('cpf') }}" required>
          @if ($errors->has('cpf'))
            <span class="help-block">
              <strong>{{ $errors->first('cpf') }}</strong>
            </span>
          @endif
        </div>
      </div>
      <div class="form-group{{ $errors->has('login') ? ' has-error' : '' }}">
        <label for="login" class="col-md-4 control-label">Login </label>
        <div class="col-md-6">
          <input id="login" type="text" class="form-control" name="login" value="{{ old('login') }}" required>
          @if ($errors->has('login'))
            <span class="help-block">
              <strong>{{ $errors->first('login') }}</strong>
            </span>
          @endif
        </div>
      </div>
      <div class="form-group{{ $errors->has('login') ? ' has-error' : '' }}">
        <label for="email" class="col-md-4 control-label">Email </label>
        <div class="col-md-6">
          <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" required>
          @if ($errors->has('email'))
            <span class="help-block">
              <strong>{{ $errors->first('login') }}</strong>
            </span>
          @endif
        </div>
      </div>
      <div class="form-group{{ $errors->has('endereco') ? ' has-error' : '' }}">
        <label for="endereco" class="col-md-4 control-label">Endereco </label>
        <div class="col-md-6">
          <input id="endereco" type="text" class="form-control" name="endereco" value="{{ old('endereco') }}">
          @if ($errors->has('endereco'))
            <span class="help-block">
              <strong>{{ $errors->first('endereco') }}</strong>
            </span>
            @endif
        </div>
      </div>
      @unless ($Empresa->count())
      @else
        <div class="panel panel-semborda">
          <!-- Vincular Empresas no Cadastro de Usuário -->
          <div class="panel-heading">Cadastrar Usuário a Uma Empresa?</div>
          <table class="table table-striped">
            @foreach ($Empresa as $Empresa)
              @php
                $teste = $loop->iteration % 2;
              @endphp
              @if ($teste == 1)
                <tr>
              @endif
                  <td>
                    <input id type="checkbox" name = "empresa_id[{{$Empresa->id}}]" value="{{$Empresa->id}}"> {{$Empresa->name}}
                  </td>
              @if ($teste == 0)
                </tr>
              @endif
            @endforeach
            @if ($teste == 1)
              <td>&nbsp;</td></tr>
            @endif
          </table>
        </div>
      @endunless
      <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
          <button type="submit" class="btn btn-primary">
            <i class="fa fa-save"></i> Salvar
          </button>
        </div>
      </div>
    </form>
  </div>
</div>
@endsection
