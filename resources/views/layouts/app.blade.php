<!-- Estilo principal da página -->
<html>
  <head>
    <link href="{{asset('css/app.css')}}" rel = "stylesheet">
    <title> Desafio BRy </title>
    <meta name="csrf-token" content="{{csrf_token()}}">
    <style>
      body
      {
        padding: 10px;
      }
      .navbar
      {
        margin-bottom: 5px;
      }
    </style>
  </head>
  <body>
    <div class = "container">
      @component('navbar', ["current" => $current])
      @endcomponent
      <main role = "main">
        @hasSection('body')
          @yield('body')
        @endif
      </main>
    </div>
    <script src="{{asset('js/app.js')}}" type="text/javascript"></script>
    @hasSection('javascript')
      @yield('javascript')
    @endif
  </body>
</html>
