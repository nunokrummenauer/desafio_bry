@extends('layouts.app', ["current" => "empresas"])
@section('body')
<div class="card border">
  <div class="card-body">
    <h5 class="card-title">Empresas Cadastradas</h5>
    <div class="table-responsive">
      <table class="table table-ordered table-hover">
        <tr>
          <th>Nome</th>
          <th>CNPJ</th>
          <th></th>
          <th></th>
          <th ></th>
        </tr>
        @foreach ($Empresa as $Empresa)
          <tr>
            <td> {{$Empresa->name}}</td>
            <td> {{$Empresa->cnpj}}</td>
            <td class="text-center">
              <a href= "{{ route('empresa.edit',$Empresa->id) }}" class="btn btn-sm">
                Editar</a>
            </td>
            <td class="text-center">
              <a href= "{{ route('empresa.show',$Empresa->id) }}" class="btn btn-sm">
                Informações</a>
            </td>
            <td class="text-center">
              {{method_field('DELETE')}}
              <a href= "{{ route('empresa.destroy',$Empresa->id) }}" value="Delete" class="btn btn-sm">
              Excluir</a>
            </td>
          </tr>
        @endforeach
      </table>
      <a class="btn btn-danger" href="{{ route('empresa.create') }}">Cadastrar Nova Empresa</a>
    </div>
  </div>
</div>
@endsection
@section('javascript')
  <script type="text/javascript">

  </script>
@endsection
