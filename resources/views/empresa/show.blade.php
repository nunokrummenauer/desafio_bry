@extends('layouts.app', ["current" => "empresas"])
@section('body')
<div class="card border">
  <div class="card-body">
    <h5 class="card-title">Dados da Empresa</h5>
    <form class="form-horizontal" role="form" method="POST" action="#">
      <div class="form-group">
        <label for="user" class="col-md-4 control-label">Razão Social (Nome)</label>
        <label for="user" class="col-md-6 control-label text-left">{{ $Empresa->name }}</label>
      </div>
      <div class="form-group">
        <label for="nome" class="col-md-4 control-label">CNPJ</label>
        <label for="nome" class="col-md-6 control-label text-left">{{ $Empresa->cnpj }}</label>
      </div>
      <div class="form-group">
        <label for="nome" class="col-md-4 control-label">Endereço</label>
        <label for="nome" class="col-md-6 control-label text-left">{{ $Empresa->endereco }}</label>
      </div>
    </form>
  </div>
</div>
@endsection
