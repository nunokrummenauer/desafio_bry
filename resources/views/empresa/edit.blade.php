@extends('layouts.app', ["current" => "empresas"])
@section('body')
<div class="card border">
  <div class="card-body">
    <form class="form-horizontal" method="POST" action="{{ route('empresa.update',$Empresa->id) }}">
      @csrf
      @method('PUT')
      <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        <label for="name" class="col-md-4 control-label">Nome </label>
        <div class="col-md-6">
          <input id="name" type="text" class="form-control" name="name" value="{{$Empresa->name}}" required>
          @if ($errors->has('name'))
            <span class="help-block">
              <strong>{{ $errors->first('name') }}</strong>
            </span>
          @endif
        </div>
      </div>
      <div class="form-group{{ $errors->has('cnpj') ? ' has-error' : '' }}">
        <label for="cnpj" class="col-md-4 control-label">CNPJ </label>
        <div class="col-md-6">
          <input id="CNPJ" type="text" class="form-control" name="cnpj" value="{{$Empresa->cnpj}}" required>
          @if ($errors->has('cnpj'))
            <span class="help-block">
              <strong>{{ $errors->first('cnpj') }}</strong>
            </span>
          @endif
        </div>
      </div>
      <div class="form-group{{ $errors->has('endereco') ? ' has-error' : '' }}">
        <label for="descricao" class="col-md-4 control-label">Endereco </label>
        <div class="col-md-6">
          <input id="endereco" type="text" class="form-control" name="endereco" value="{{$Empresa->endereco}}">
          @if ($errors->has('endereco'))
            <span class="help-block">
              <strong>{{ $errors->first('endereco') }}</strong>
            </span>
          @endif
        </div>
      </div>
      <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
          <button type="submit" class="btn btn-primary">
            <i class="fa fa-save"></i> Salvar
          </button>
        </div>
      </div>
    </form>
  </div>
</div>
@endsection
