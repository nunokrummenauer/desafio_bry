@extends('layouts.app', ["current" => "home"])

@section('body')
  <div class="jumbotron bg-light border border-secondary">
      <h5 class="card-title">Desafio BRy Tecnologia</h5>
      <!--
          Havia entendido errado a proposta. Como não havia tempo viável,
          fiz um redirecionamento para desenvolvero possível da aplicação em angular,
          mas mantendo a aplicação inicial.
      -->
      <p class="card=text">Estamos atualizando essa aplicação para Angular!</p><a class="nav-link" href="/angular">Clique Aqui para Visualizar</a>

  </div>

@endsection
