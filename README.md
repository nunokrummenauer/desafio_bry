Comandos para preparação do projeto:
Criação do projeto: 
  -> laravel new desafio_BRy
  Dentro do Diretório do Projeto:
    -> npm install
    ->npm run dev
Banco de dados: MySQL
    ->Nome do SCHEMA: desafio_BRy
    ->Usuário do BD: root
    ->Senha: 
após isso, basta fazer o git pull -u origin master para puxar os dados do GIT.

Postman Collection Empresas
https://www.getpostman.com/collections/7bb575d085b3c396df1b

Postman Collection Users
https://www.getpostman.com/collections/d4a45c9d1f109a01d722

!!IMPORTANTE!!
Tive alguns problemas de saúde pessoal no final de semana que atrapalharam meu foco. 
Então não consegui dar o melhor que eu posso, mas fiz o melhor que conseguia no momento.
Acabei entendendo a proposta inicial do desafio de forma equivocada e realizei incialmente um FRONT em Laravel com Bootstrap.
Iria conectar algumas funções de formulários com Angular a ele, como utilizamos o VUE na empresa. 
Depois de reler o desafio algumas vezes, percebi que era para criar a aplicação Angular e conectar na API em Laravel, porém não houve tempo para refazer.
Deixei as duas rodando, com alguns comentários e as controllers da API e dos formulários de forma separada. A parte do Angular não consegui dar vasão e praticamente só integrei com o Laravel, sem conseguir criar nada.
Isso acabou deixando a aplicação um pouco bagunçada, mas não consegui corrigir no tempo hábil.
Estou enviando dessa forma, porque queria entregar o desafio nem que fosse de forma parcial, pois eu me interessei muito pela vaga e fiquei chateado de não ter conseguido dar o meu melhor na tarefa.

Para vincular Usuários e Empresas, está funcional apenas via formulários HTTP do Laravel.
