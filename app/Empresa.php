<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $fillable = [
        'name','cnpj','endereco',
    ];

    public function users () {
        return $this->belongsToMany('App\User', 'empresa_users');
    }

}
