<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable = [
        'name','cpf','login','email','endereco',
    ];

    public function empresas () {
        return $this->belongsToMany('App\Empresa', 'empresa_users');
    }
}
