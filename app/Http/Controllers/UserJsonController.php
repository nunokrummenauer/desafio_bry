<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Empresa;
use App\EmpresaUser;

class UserJsonController extends Controller
{
    public function index()
    {
        //Get para Buscar dados de Empresa
        $User = User::with("empresas")->get();
        return json_encode($User);
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        // Criar regras de validação básicas.
        $rules = [
            'name' => 'required',
            'cpf' => 'required|digits:11|numeric|unique:users',
            'email' => 'required|email',
            'login' => 'required|unique:users',
        ];
        $messages = [
            'required' => ':attribute é obrigatório',
            'digits' => 'caractéres de :attribute não são válidos',
            'numeric' => ':attribute deve conter apenas números',
            'unique' => ':attribute já existe no sistema',
        ];
        $request->validate($rules,$messages);
        $User = new User($request->all());
        $User->save();
        return json_encode($User);
    }
    public function show($id)
    {
        //Get para Buscar dados de Empresa 
        $User = User::with("empresas")->where('id',$id)->get();
        return json_encode($User);
    }
    public function edit($id)
    {
        //
    }
    public function update(Request $request, $id)
    {
        // Criar regras de validação básicas.
        $rules = [
            'name' => 'required',
            'cpf' => 'required|digits:11|numeric',
            'email' => 'required|email',
            'login' => 'required',
        ];
        $messages = [
            'required' => ':attribute é obrigatório',
            'digits' => 'caractéres de :attribute não são válidos',
            'numeric' => ':attribute deve conter apenas números',
            'unique' => ':attribute já existe no sistema',
            'email.email' => 'digite um email válido',
        ];
        $request->validate($rules,$messages);
        $User = User::find($id)->update($request->all());
        return json_encode($User);
    }
    public function destroy($id)
    {
        $User = User::find($id);
        if (isset($User)) {
          $User->delete();
          return response ('Usuario de id'.$id.' excluído', 200);
        }
        return response ('Não foi possível excluir o produto', 404);
    }
}
