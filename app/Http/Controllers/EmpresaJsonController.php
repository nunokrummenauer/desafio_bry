<?php

namespace App\Http\Controllers;

use App\User;
use App\Empresa;
use App\EmpresaUser;

use Illuminate\Http\Request;

class EmpresaJsonController extends Controller
{
    public function index()
    {
        //Get para Buscar dados de Usuário
        $Empresa = Empresa::with("users")->get();
        return json_encode($Empresa);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
      // Criar regras de validação básicas.
      $rules = [
          'name' => 'required',
          'cnpj' => 'required|digits:14|numeric',
      ];
      $messages = [
          'required' => ':attribute é obrigatório',
          'digits' => 'caractéres de :attribute não são válidos',
          'numeric' => ':attribute deve conter apenas números',
          'unique' => ':attribute já existe no sistema',
      ];
      $request->validate($rules,$messages);
      $Empresa = new Empresa($request->all());
      $Empresa->save();
      return json_encode($Empresa);
    }

    public function show($id)
    {
        //Get para Buscar dados de Usuário
        $Empresa = Empresa::with("users")->where('id',$id)->get();
        return json_encode($Empresa);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        // Criar regras de validação básicas.
        $rules = [
            'name' => 'required',
            'cnpj' => 'required|digits:14|numeric',
        ];
        $messages = [
            'required' => ':attribute é obrigatório',
            'digits' => 'caractéres de :attribute não são válidos',
            'numeric' => ':attribute deve conter apenas números',
            'unique' => ':attribute já existe no sistema',
        ];
        $request->validate($rules,$messages);
        $Empresa = Empresa::find($id)->update($request->all());
        return json_encode($Empresa);
    }

    public function destroy($id)
    {
        $Empresa = Empresa::find($id);
        if (isset($Empresa)) {
          $Empresa->delete();
          return response ('OK', 200);
        }
        return response ('Não foi possível excluir o produto', 404);
    }
}
