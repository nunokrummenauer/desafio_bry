<?php

namespace App\Http\Controllers;
use App\User;
use App\Empresa;
use App\EmpresaUser;
use DB;
use Illuminate\Http\Request;

class UserController extends Controller
{

  public function index()
  {
      $User = User::orderBy('name','asc')->get();
      return view('user.index', ['User' => $User]);
  }

  public function create()
  {
      $Empresa = Empresa::get();
      return view ('user.create', ['Empresa' => $Empresa]);
  }


  public function store(Request $request)
  {
      // Criar regras de validação básicas.
      $rules = [
          'name' => 'required',
          'cpf' => 'required|digits:11|numeric|unique:users',
          'email' => 'required|email',
          'login' => 'required|unique:users',
      ];
      $messages = [
          'required' => ':attribute é obrigatório',
          'digits' => 'caractéres de :attribute não são válidos',
          'numeric' => ':attribute deve conter apenas números',
          'unique' => ':attribute já existe no sistema',
      ];
      $request->validate($rules,$messages);
      $User = new User($request->all());
      $User->save();
      $empresa_id = $request->get('empresa_id');
      foreach ($empresa_id as $emp_id)
      {
        $EmpresaUser = New EmpresaUser;
        $EmpresaUser->empresa_id = $emp_id;
        $EmpresaUser->user_id = $User->id;
        $EmpresaUser->save();
      }
      return redirect()->route('user.index');
  }

  public function show($id)
  {
    //Get para Buscar dados de Empresa
    $User = User::find($id);
    return view('user.show', ['User' => $User]);

  }

  public function edit($id)
  {
    $User = User::find($id);
    $Empresa = Empresa::get();
    return view('user.edit',['User' => $User], ['Empresa' => $Empresa]);
  }

  public function update(Request $request, $id)
  {
      // Criar regras de validação básicas.
      $rules = [
          'name' => 'required',
          'cpf' => 'required|digits:11|numeric',
          'email' => 'required|email',
          'login' => 'required',
      ];
      $messages = [
          'required' => ':attribute é obrigatório',
          'digits' => 'caractéres de :attribute não são válidos',
          'numeric' => ':attribute deve conter apenas números',
          'unique' => ':attribute já existe no sistema',
          'email.email' => 'digite um email válido',
      ];
      $request->validate($rules,$messages);
      $User = User::find($id)->update($request->all());
      return redirect()->route('user.index');
  }

  public function destroy($id)
  {
      $User = User::find($id);
      if (isset($User)) {
        $User->delete();
        return response ('OK', 200);
      }
      return response ('Não foi possível excluir o produto', 404);
  }

}
