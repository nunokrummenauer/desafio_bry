<?php

namespace App\Http\Controllers;

use App\User;
use App\Empresa;
use App\EmpresaUser;

use Illuminate\Http\Request;

class EmpresaController extends Controller
{

  public function index()
  {
      $Empresa = Empresa::orderBy('name','asc')->get();
      return view('empresa.index', ['Empresa' => $Empresa]);
  }

  public function create()
  {
      $User = User::get();
      return view ('empresa.create', ['User' => $User]);
  }


  public function store(Request $request)
  {
      $rules = [
          'name' => 'required',
          'cnpj' => 'required|digits:14|numeric|unique:empresas',
      ];
      $messages = [
          'required' => ':attribute é obrigatório',
          'digits' => 'caractéres de :attribute não são válidos',
          'numeric' => ':attribute deve conter apenas números',
          'unique' => ':attribute já existe no sistema',
      ];
      $request->validate($rules,$messages);
      $Empresa = new Empresa($request->all());
      $Empresa->save();
      $user_id = $request->get('user_id');
      foreach ($user_id as $usr_id)
      {
        $EmpresaUser = New EmpresaUser;
        $EmpresaUser->empresa_id = $Empresa->id;
        $EmpresaUser->user_id = $usr_id;
        $EmpresaUser->save();
      }
      return redirect()->route('empresa.index');
  }

  public function show($id)
  {
    $Empresa = Empresa::find($id);
    return view('empresa.show', ['Empresa' => $Empresa]);
  }

  public function edit($id)
  {
      $Empresa = Empresa::find($id);
      return view('empresa.edit',['Empresa' => $Empresa]);
  }

  public function update(Request $request, $id)
  {
      $rules = [
          'name' => 'required',
          'cnpj' => 'required|digits:14|numeric',
      ];
      $messages = [
          'required' => ':attribute é obrigatório',
          'digits' => 'caractéres de :attribute não são válidos',
          'numeric' => ':attribute deve conter apenas números',
          'unique' => ':attribute já existe no sistema',
      ];
      $request->validate($rules,$messages);
      $Empresa = Empresa::find($id)->update($request->all());
      return redirect()->route('empresa.index');
  }

  public function destroy($id)
  {
      $Empresa = Empresa::find($id);
      if (isset($Empresa)) {
        $Empresa->delete();
        return response ('OK', 200);
      }
      return response ('Não foi possível excluir o produto', 404);
  }

}
