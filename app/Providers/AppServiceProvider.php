<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    public function register()
    {

    }

    public function boot()
    {
              //Tratamento para corrigir conjunto de caracteres para versão 5.7.21 do MySQL
              Schema::defaultStringLength(191);
    }
}
